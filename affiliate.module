<?php

/**
 * Implements hook_init().
 */
function affiliate_init() {
  module_load_include('inc', 'affiliate');
  affiliate_add_js();
}

/**
 * Implements hook_permission().
 */
function affiliate_permission() {
  return array(
    'administer affiliate settings' => array(
      'title' => t('Administer affiliate settings'),
      'description' => t('Allow users to administer the site-wide configuration for the affiliate module.'),
    ),
    'administer own affiliate settings' => array(
      'title' => t('Administer own affiliate settings'),
      'description' => t('Allow users to administer their personal affiliate settings.'),
    ),
    'view any affiliate info' => array(
      'title' => t('View any affiliate info'),
      'description' => t('Allow users to access the affiliate information of any user on the site, including payouts owed, payouts paid, etc.'),
    ),
    'view affiliate overviews' => array(
      'title' => t('View affiliate overviews'),
      'description' => t('Allow users to access affiliate summary reports and information, such as top-ranking affiliates, top climbers, etc.'),
    ),
    'view own affiliate info' => array(
      'title' => t('View own affiliate info'),
      'description' => t('Allow users to access their own affiliate information, such as statistics, payouts earned, etc.'),
    ),
    'opt in or out as an affiliate' => array(
      'title' => t('Opt in or out as an affiliate'),
      'description' => t('Allow users to opt-in as an affiliate by displaying an opt-in option on their account page.'),
    ),
    'track affiliate clicks for this role' => array(
      'title' => t('Track affiliate clicks for this role'),
      'description' => t('Only the roles selected will be tracked by the affiliate module.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function affiliate_menu() {
  // Allow admins to provide a custom path for their affiliate links.
  $affiliate_menu_path = variable_get('affiliate_module_affiliate_menu_path', 'affiliate');
  $items[$affiliate_menu_path] = array(
    'page callback' => 'affiliate_page',
    'access arguments' => array('track affiliate clicks for this role'),
    'type' => MENU_CALLBACK,
  );
  $items['affiliate/top-users'] = array(
    'title' => 'Top affiliates',
    'page callback' => 'affiliate_top_users_page',
    'access arguments' => array('view affiliate overviews'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['user/%user/affiliate'] = array(
    'title' => 'Affiliate info',
    'page callback' => 'affiliate_user_page',
    'page arguments' => array(1),
    'access arguments' => array('view own affiliate info'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  $items['admin/config/people/affiliate'] = array(
    'title' => 'Affiliate management',
    'access arguments' => array('administer affiliate settings'),
    'description' => 'Manage affiliate settings',
    'page callback' => 'affiliate_admin_overviewpage',
  );
  $items['admin/config/people/affiliate/overview'] = array(
    'title' => 'Overview',
    'access arguments' => array('administer affiliate settings'),
    'page callback' => 'affiliate_admin_overviewpage',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/config/people/affiliate/users'] = array(
    'title' => 'Users',
    'access arguments' => array('administer affiliate settings'),
    'page callback' => 'affiliate_admin_userspage',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/config/people/affiliate/content'] = array(
    'title' => 'Content',
    'access arguments' => array('administer affiliate settings'),
    'page callback' => 'affiliate_admin_contentpage',
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  $items['admin/config/people/affiliate/payouts'] = array(
    'title' => 'Payouts',
    'access arguments' => array('administer affiliate settings'),
    'page callback' => 'affiliate_admin_payoutspage',
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );
  return $items;
}

/**
 * Implements hook_node_view().
 */
function affiliate_node_view($node, $view_mode, $langcode = NULL) {
  global $user;
  $is_eligible = affiliate_node_is_eligible($node);
  if ($user && $user->uid && isset($user->data['affiliate_optin']) && $user->data['affiliate_optin'] && $view_mode == 'full' && $is_eligible) {
    $url = affiliate_fetch_url($user->uid);
    $node->content['affiliate'] = array(
      '#markup' => theme('affiliate', array('url' => $url, 'display' => NULL, 'node' => $node)),
      '#weight' => 100,
    );
  }
}

/**
 * Implements hook_theme().
 */
function affiliate_theme() {
  return array(
    'affiliate' => array(
      'variables' => array('url' => NULL, 'display' => NULL, 'node' => NULL),
      'template' => 'affiliate',
    ),
  );
}

/**
 * Implements hook_user_presave().
 */
function affiliate_user_presave(&$edit, $account, $category) {
  $edit['data']['affiliate_optin'] = isset($edit['affiliate_optin']) ? $edit['affiliate_optin'] : 0;
  $edit['data']['affiliate_homepage'] = isset($edit['affiliate_homepage']) ? $edit['affiliate_homepage'] : FALSE;
}

/**
 * Implements hook_user_view().
 */
function affiliate_user_view($account) {
  global $user;
  if (user_access('view affiliate overviews') || user_access('administer affiliate settings') || user_access('view any affiliate info') || ($account->uid == $user->uid && (user_access('administer own affiliate settings') || user_access('view own affiliate info', $account))) && isset($account->data['affiliate_optin']) && $account->data['affiliate_optin']) {
    $account->content['affiliate'] =  array(
      '#type' => 'user_profile_category',
      '#title' => t('Affiliate info'),
      '#attributes' => array('class' => array('affiliate-profile')),
      '#weight' => 11,
    );
    if (isset($account->data['affiliate_homepage']) && !empty($account->data['affiliate_homepage'])) {
      $homepage = url($account->data['affiliate_homepage']);
      $account->content["affiliate"]["affiliate_homepage"] =  array(
        "#type" => "user_profile_item",
        "#title" => t("Web site"),
        "#markup" => l($homepage, $homepage, array("attributes" => array("title" => t("Visit !username's web site", array("!username" => format_username($account)))))),
        "#attributes" => array("class" => array("affiliate-profile", "affiliate-profile-homepage")),
      );
    }
    if (user_access('administer affiliate settings') || user_access('view any affiliate info') || ($account->uid == $user->uid && (user_access('administer own affiliate settings') || user_access('view own affiliate info', $account)))) {
      $account->content['affiliate']['affiliate_history'] =  array(
        '#type' => 'user_profile_item',
        '#title' => t('Affiliate history'),
        '#markup' => l(t('View affiliate history'), "user/$account->uid/affiliate", array('attributes' => array('title' => t('View affiliate information and history for !username', array('!username' => format_username($account)))))),
        '#attributes' => array('class' => array('affiliate-profile', 'affiliate-profile-history')),
      );
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function affiliate_form_user_profile_form_alter(&$form, &$form_state) {
  if (user_access('administer affiliate settings') || user_access('administer own affiliate settings') || user_access('opt in or out as an affiliate')) {
    if ($form['#user_category'] == 'account') {
      $account = $form['#user'];
      $form['#validate'][] = 'affiliate_user_profile_form_validate';
      $form['#submit'][] = 'affiliate_user_profile_form_submit';
      $form['affiliate'] = array(
        '#type' => 'fieldset',
        '#title' => t('Affiliate settings'),
        '#weight' => 5,
        '#collapsible' => TRUE,
      );
      $form['affiliate']['affiliate_optin'] = array(
        '#type' => 'checkbox',
        '#title' => t('Affiliate opt-in'),
        '#default_value' => !empty($account->data['affiliate_optin']) ? $account->data['affiliate_optin'] : FALSE,
        '#description' => t('Opt in to affiliate services?'),
      );
      if (user_access('administer own affiliate settings') || user_access('administer affiliate settings')) {
        $form['affiliate']['affiliate_homepage'] = array(
          '#type' => 'textfield',
          '#title' => t('Home page'),
          '#default_value' => !empty($account->data['affiliate_homepage']) ? $account->data['affiliate_homepage'] : FALSE,
          '#description' => t('Provide the URL of the website associated with this account. Format: http://www.example.com'),
        );
      }
    }
  }
}

/**
 * Implements hook_block_info().
 */
function affiliate_block_info() {
  $blocks['affiliate_top_users']['info'] = t('Top affiliates');
  return $blocks;
}

function affiliate_block_list() {
  $blocks['affiliate_top_users'] = array(
    'info'       => t('Top affiliates'),
    'weight'     => 0,
  );
  return $blocks;
}

function affiliate_block_view($delta = 0) {
  if (user_access('view affiliate overviews') || user_access('administer affiliate settings') || user_access('view any affiliate info')) {
    $block = array();
    switch ($delta) {
      case 'affiliate_top_users':
        $block['subject'] = t('Top affiliates');
        $block['content'] = affiliate_top_users_block_content();
      break;
    }
  return $block;
  }
}

/**
 * Implements hook_cron().
 */
function affiliate_cron() {
  affiliate_bulk_create_node_campaigns();
  affiliate_update_affiliate_clicks();
  affiliate_update_affiliate_referrals();
}

/**
 * Form validation function for roku_form_user_profile_form_alter().
 */
function affiliate_user_profile_form_validate($form, $form_state) {
  if (isset($form_state['values']['affiliate_optin']) && empty($form_state['values']['affiliate_optin']) && isset($form_state['values']['affiliate_homepage']) && !empty($form_state['values']['affiliate_homepage'])) {
    form_set_error('affiliate_optin', t('You must opt in as an affiliate before you can save your affiliate home page.'));
  }
}

/**
 * Form submission function for affiliate_form_user_profile_form_alter().
 */
function affiliate_user_profile_form_submit($form, $form_state) {
  $uid = $form_state['values']['uid'];
  if (isset($form_state['values']['affiliate_optin'])) {
    $edit_affiliate = (int) $form_state['values']['affiliate_optin'];
    $fetch_affiliate = affiliate_load_affiliate($uid);
    $affiliate_uid = isset($fetch_affiliate->uid) ? $fetch_affiliate->uid : 0;
    if ($edit_affiliate < 1) {
      affiliate_set_affiliate_status($uid, 0);
    }
    else if ($edit_affiliate > 0 && $affiliate_uid) {
      affiliate_set_affiliate_status($uid, 1);
    }
    else if ($edit_affiliate > 0) {
      affiliate_insert_affiliate($uid);
    }
  }
}

function affiliate_payout_inlineform($form, $form_state, $uid) {
  $uid = $uid;
  $form['#validate'][] = 'affiliate_payout_inlineform_validate';
  $form['#submit'][] = 'affiliate_payout_inlineform_submit';
  $form['affiliate_payout_uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid,
  );
  $form['paid-' . $uid] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="affiliate-payout-inlineform-submit" id="paid-' . $uid . '">',
    '#suffix' => '</div>',
  );
  $form['paid-' . $uid]['affiliate_payout_amount'] = array(
    '#prefix' => '<div class="affiliate-payout-inlineform-amount">',
    '#type' => 'textfield',
    '#title' => '',
    '#default_value' => 0.00,
    '#size' => 5,
    '#suffix' => '</div>',
  );
  $form['affiliate_payout_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply payment'),
    '#ajax' => array(
      'callback' => 'affiliate_payout_inlineform_ajaxsubmit',
      'wrapper' => 'paid-' . $uid,
    ),
  );
  return $form;
}

function affiliate_payout_inlineform_validate($form, $form_state) {
  if (!user_access('administer affiliate settings') || !user_access('administer affiliate payouts')) {
    return;
  }
  $uid = (int) $form_state['values']['affiliate_payout_uid'];
  $uid_is_affiliate = db_query("SELECT COUNT(active) FROM {affiliate} WHERE uid = :uid", array(":uid" => $uid))->fetchField();
  if (!$uid || !$uid_is_affiliate) {
    form_set_error('affiliate_payout_uid', t('Not a valid affiliate.'));
  }
}

function affiliate_payout_inlineform_submit($form, $form_state) {
  $uid = (int) $form_state['values']['affiliate_payout_uid'];
  $amount_owed_old = db_query("SELECT payouts_owed FROM {affiliate} WHERE uid = :uid", array(":uid" => $uid))->fetchField();
  $amount_owed_old = number_format((float)$amount_owed_old, 2, '.', '');
  $amount_paid_old = db_query("SELECT payouts_paid FROM {affiliate} WHERE uid = :uid", array(":uid" => $uid))->fetchField();
  $amount_paid_old = number_format((float)$amount_paid_old, 2, '.', '');
  $amount_paid_now = $form_state['values']['affiliate_payout_amount'];
  $amount_paid_now = number_format((float)$amount_paid_now, 2, '.', '');
  // Amounts can be negative... Change?
  $amount_owed_new = $amount_owed_old - $amount_paid_now;
  $amount_owed_new = number_format((float)$amount_owed_new, 2, '.', '');
  $amount_paid_new = $amount_paid_old + $amount_paid_now;
  $amount_paid_new = number_format((float)$amount_paid_new, 2, '.', '');
  db_update('affiliate')
    ->fields(array(
      'payouts_owed' => $amount_owed_new,
      'payouts_paid' => $amount_paid_new,
    ))
    ->condition('uid', $uid)
  ->execute();
}

function affiliate_payout_inlineform_ajaxsubmit($form, $form_state) {
  $affid = (int) $form_state['values']['affiliate_payout_uid'];
  $amount = $form_state['values']['affiliate_payout_amount'];
  $element_id = 'paid-' . $affid;
  $element = $form[$element_id];
  $element["#markup"] = t("%amount applied", array("%amount" => $amount));
  return $element;
}

function affiliate_add_js() {
  $js = drupal_get_path('module', 'affiliate') . '/affiliate.js';
  drupal_add_js($js);
}
